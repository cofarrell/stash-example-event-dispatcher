package com.atlassian.stash.plugin.events;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginContainerRefreshedEvent;
import com.atlassian.stash.event.request.RequestEvent;
import com.atlassian.utils.process.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class EventDispatcher implements InitializingBean, DisposableBean {

    private EventPublisher eventPublisher;

    public EventDispatcher(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    public void onEvent(Object event) {
        // Ignore these - they don't serialize very well
        if (event instanceof RequestEvent || event instanceof PluginContainerRefreshedEvent) {
            return;
        }
        publishEvent(event);
    }

    private void publishEvent(Object event) {
        try {
            URL url = new URL(System.getProperty("event.url", "http://localhost:8000"));
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStream out = connection.getOutputStream();
            InputStream in = connection.getInputStream();
            try {
                serialiseEvent(event, out);
            } finally {
                IOUtils.closeQuietly(in);
                IOUtils.closeQuietly(out);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void serialiseEvent(Object event, OutputStream out) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SerializationConfig config = objectMapper.getSerializationConfig()
                    .without(SerializationConfig.Feature.USE_ANNOTATIONS);
            objectMapper.setSerializationConfig(config);
            objectMapper.writeValue(out, event);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
